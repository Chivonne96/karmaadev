from django.contrib import admin
from .models import Profile,LinkedinProfile,UserSubscription


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'date_of_birth', 'photo']
    
class LinkedinModelAdmin(admin.ModelAdmin):
    list_display = ['first_name','email', 'industry']

class UserSubscriptionAdmin(admin.ModelAdmin):
    list_display = ['moduleName', 'beginDate','endDate']
                    
admin.site.register(Profile, ProfileAdmin)
admin.site.register(LinkedinProfile, LinkedinModelAdmin)
admin.site.register(UserSubscription,UserSubscriptionAdmin)
