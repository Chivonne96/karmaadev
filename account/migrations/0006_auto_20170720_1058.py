# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-20 10:58
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0005_auto_20170720_1050'),
    ]

    operations = [
        migrations.RenameField(
            model_name='usersubscription',
            old_name='name',
            new_name='moduleName',
        ),
        migrations.AlterField(
            model_name='profile',
            name='joinedDate',
            field=models.DateTimeField(default=datetime.datetime(2017, 7, 20, 10, 58, 11, 573329)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='trialEndDate',
            field=models.DateTimeField(default=datetime.datetime(2017, 7, 27, 10, 58, 11, 573393)),
        ),
        migrations.AlterField(
            model_name='usersubscription',
            name='beginDate',
            field=models.DateField(default=datetime.datetime(2017, 7, 20, 10, 58, 11, 574948)),
        ),
        migrations.AlterField(
            model_name='usersubscription',
            name='endDate',
            field=models.DateTimeField(default=datetime.datetime(2017, 7, 20, 10, 58, 11, 574997)),
        ),
    ]
