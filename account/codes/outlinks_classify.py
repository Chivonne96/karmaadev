
def classify(soup, main_url, list_of_links):
	
	import re
	import requests
	#p1=re.compile("\.?[a-zA-Z0-9\-]+\.(com|org|net|mil|edu|COM|ORG|NET|MIL|EDU)?")          #get the base website url out of it
	p1=re.compile("\.[a-zA-Z0-9\-]+\.")
	p2=re.compile(".+jpg.+|.+png.+|.+jpeg.+")	
	check_pinterest	= re.compile(".+pinterest.+")	
	m1=p1.search(main_url)		#use  search() coz match() looks at the beginning of the string
	try:
		base_url=m1.group()
		base = base_url.replace(".","")
		same_site_links = list()
		diff_site_links = list()
		image_links=list()
		
		for link in list_of_links:		#now compare all other urls to the base url. if base url is present...blah blah

			
			if not link:
				pass
			else:
				if base in link:
					m2 = p2.search(link)
					if m2:
						response = requests.head(link)			#check if the link exists or not. Nonexistent links cause annoying alignment issues when displaying
						if response.status_code != 404:
							if check_pinterest.search(link):
								pass
							else:
								image_links.append(link)
						else:
							pass
					else:
						same_site_links.append(link)
				else:
					diff_site_links.append(link)	

		try:
			pic = soup.find("meta",{"property":"og:image"})['content']
			if check_pinterest.search(pic):
				pass
			else:
				image_links.append(soup.find("meta",{"property":"og:image"})['content'])
		except:
			pass		
		
		img_tags = soup.find_all("img")
		for link in img_tags:
			m3 = p2.search(link['src'])
			if m3:
				m4 = check_pinterest.search(link['src'])
				if m4:
					pass
				else:
					image_links.append(link['src'])         #all possible image links

		return (same_site_links,diff_site_links,image_links)
	except:
		return([],[],[])	