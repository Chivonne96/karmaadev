def mainfunc(main_url):
	import requests
	from urllib.request import urlopen
	from bs4 import BeautifulSoup
	import re
	import nltk
	import enchant
	from nltk.corpus import stopwords
	from nltk.tokenize import word_tokenize
	import newspaper, lassie
	from newspaper import Article

	page=urlopen(main_url)
	soup=BeautifulSoup(page,'html.parser')


	newspaper_data = Article(main_url)		#got data through newspaper module
	newspaper_data.download()
	newspaper_data.parse()
	newspaper_data.nlp()

	lassie_data = lassie.fetch(main_url)	#got data through lassie module


	"""		this snippet is added to outlinks_classify
	try:
		soup.find("meta",{"property":"og:image"})['content']
		product_image = soup.find("meta",{"property":"og:image"})['content']
	except:
		pass
	"""
	try:
		soup.find("meta",{"property":"og:price:amount"})['content']
		price = soup.find("meta",{"property":"og:price:amount"})['content']
	except:
		price="Sorry. Not found."


	title=soup.title.text
	title_words=title.split()
	dd=enchant.Dict("en_GB")
	list_of_possible_pid=[]
	for word in title_words:
	    if not dd.check(word):
	        list_of_possible_pid.append(word)
	    else:
	        pass
	product_id=list_of_possible_pid[-1] #saw on Jabong that the SKU id is the last item on this list.

	try:
		if soup.find("div",class_="detail"):
			desc = soup.find("div",class_="detail")
			product_description = desc.text
		else:
			lassie_desc = str(lassie_data['description'])
			newspaper_summ = str(newspaper_data.summary)
			product_description = lassie_desc + newspaper_summ
	except:
		product_description = "Not found"

	
	prod_image = newspaper_data.top_image

	return(price, product_id, product_description, prod_image)