def summ_cate(url,soup):
	from nltk.tokenize import sent_tokenize,word_tokenize
	from nltk.corpus import stopwords
	from collections import defaultdict
	from string import punctuation
	from heapq import nlargest
	import nltk
	import os
	import yaml
	from urllib.request import urlopen
	from bs4 import BeautifulSoup
	import enchant

	more_punctuation = ['|', ':']

	class FrequencySummarizer:
		def __init__(self, min_cut=0.1, max_cut=0.9):
			"""
			 Initilize the text summarizer.
			 Words that have a frequency term lower than min_cut 
			 or higer than max_cut will be ignored.
			"""
			self._min_cut = min_cut
			self._max_cut = max_cut 
			self._stopwords = set(stopwords.words('english') + list(punctuation) + more_punctuation)

		def _compute_frequencies(self, word_sent):
			""" 
			  Compute the frequency of each of word.
			  Input: 
			   word_sent, a list of sentences already tokenized.
			  Output: 
			   freq, a dictionary where freq[w] is the frequency of w.
			"""
			freq = defaultdict(int)		#defaultdict(int) means that for every new key, a default value of zero is given. We can simply do += because of this.
			for s in word_sent:
				for word in s:
					if word not in self._stopwords:
						freq[word] += 1
			# frequencies normalization and fitering
			m = float(max(freq.values()))
			for w in list(freq.keys()):
				freq[w] = freq[w]/m
				if freq[w] >= self._max_cut or freq[w] <= self._min_cut:
					del freq[w]
				if w in more_punctuation:
					del freq[w]

			
			###need to display top keywords
			top_keywords=freq 
			for w in sorted(freq, key=freq.get, reverse=True):
				flag=0			#have to remove words with non alphanumeric characters
				flag2=0
				for s in w:		#checking letters in the string if they are non alpha numeric
					if not s.isalnum() or s in more_punctuation:
						flag=1
				dd=enchant.Dict("en_GB")
				if not dd.check(w):
					flag2=1
				if flag or flag2:
					pass
				else:
					top_keywords[w]=freq[w]

			return (freq, top_keywords)              #freq is a dictionary of words we consider and their frequencies. top_keywords is the freq dict in sorted descending order

		def summarize(self, text, n):
			"""
			  Return a list of n sentences 
			  which represent the summary of text.
			"""
			sents = sent_tokenize(text)	
			n = len(sents)
			word_sent = [word_tokenize(s.lower()) for s in sents]
			self._freq, top_keywords = self._compute_frequencies(word_sent)


			ranking = defaultdict(int)
			for i,sent in enumerate(word_sent):
				for w in sent:
					if w in self._freq:
						ranking[i] += self._freq[w]
			sents_idx = self._rank(ranking, n)    
			return ([sents[j] for j in sents_idx], top_keywords)

		def _rank(self, ranking, n):
			#return the first n sentences with highest ranking
			return nlargest(n, ranking, key=ranking.get)
			
	fs=FrequencySummarizer()		
	feed_xml = urlopen(url)
	text = ' '.join(map(lambda p: p.text, soup.find_all('p')))
	summary, top_keywords=fs.summarize(text, 7)

	#------------------------CATEGORIZING BEGINS HERE
	import category
	cgory=category.cate(summary)
	return summary,cgory,top_keywords
	#------------------------CATEGORIZING ENDS HERE
