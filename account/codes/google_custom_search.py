def search_func(keywords):
	from googleapiclient.discovery import build
	import pprint,json

	my_api_key = "AIzaSyD4On4LC5MClmJZkbnyAW1-2jAgLBVhHwE"
	my_cse_id = "008775155313338422205:bhei65a8isw"

	def google_search(search_term, api_key, cse_id, **kwargs):
	    service = build("customsearch", "v1", developerKey=api_key)
	    res = service.cse().list(q=search_term, cx=cse_id, **kwargs).execute()
	    return res

	#keywords = "beautifulsoup python"
	results = google_search(keywords[:7], my_api_key, my_cse_id, num=9)
	url_list = list()
	#img_list = list()
	for item in results['items']:
	    try:
	        for metatag in item['pagemap']['metatags']:
	            try:
	                url_list.append(metatag['og:url'])
	                #img_list.append(metatag['og:image'])
	            except:
	                pass
	    except:
	        pass
	return (url_list)