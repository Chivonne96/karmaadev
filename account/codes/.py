def mainfunc(main_url):
	import requests
	from urllib.request import urlopen
	from bs4 import BeautifulSoup
	import re
	import nltk
	from nltk.corpus import stopwords
	from nltk.tokenize import word_tokenize
	import enchant
	


	page=urlopen(main_url)
	soup=BeautifulSoup(page,'html.parser')

	meta_properties=list()		#get a list of all meta properties available
	meta_tags=soup.find_all('meta')
	for tag in meta_tags:
		try:
			meta_properties.append(tag['property'])
		except:
			pass

	
	if soup.title.text:
		title = soup.title.text
	elif soup.find("meta",{"name":"description"})['content']:
		title = soup.find("meta",{"name":"description"})['content']
	else:
		title="Unknown"

	try:
		icon = soup.find("meta",{"property":"og:image"})['content']
	except:
		icon = "NA"

	if soup.time:
		date = soup.time.text
	else:	#try to get from meta tags 	
		try:
			date_prop = ""
			date_prop_pattern = re.compile(".*date.*|.*Date.*|.*DATE.*")
			for prop in meta_properties:
				m=date_prop_pattern.search(prop)
				if m:
					date_prop=prop
					break
				else:
					pass
			date = soup.find("meta",{"property":date_prop})['content']
		except:
			date = "Unknown"

		
	try:
		a_name = soup.find("div", class_="author-name")
		a_name = soup.find("div", class_="author-info")
		if a_name.text:
			author_name = a_name.text
		else:	
			author_prop = ""
			author_prop_pattern = re.compile(".*author.*|.*Author.*|.*AUTHOR.*")
			for prop in meta_properties:
				m=author_prop_pattern.search(prop)
				if m:
					author_prop=prop
					break
				else:
					pass
			author_name = soup.find("meta",{"property":author_prop})['content']
	except:
		author_name = "Unknown"
	import finalsumm
	summary,category,top_keywords=finalsumm.summ_cate(main_url,soup)
	
	keywords=list()
	title_as_list=word_tokenize(title)	
	stop_words=stopwords.words('english')
	dd = enchant.Dict("en-GB")
	for word in title_as_list:
		if (word not in stop_words and dd.check(word)):
			keywords.append(word)

	add_to_keywords=list(sorted(top_keywords, key=top_keywords.__getitem__, reverse=True))
	keywords.extend(add_to_keywords[:5])

	list_of_links = list()
	links=soup.find_all('a')
	for link in links:
		list_of_links.append(link.get('href'))

	import outlinks_classify
	same_site_links = list()
	diff_site_links = list()
	image_links = list()
	same_site_links, diff_site_links, image_links = outlinks_classify.classify(soup, main_url, list_of_links)
	return(main_url,title,date,author_name,icon,category,keywords[:8],summary,same_site_links, diff_site_links, image_links)