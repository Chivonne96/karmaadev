def mainfunc(main_url):
	import requests
	from urllib.request import urlopen
	from bs4 import BeautifulSoup
	import re
	import nltk
	from nltk.corpus import stopwords
	from nltk.tokenize import word_tokenize
	import enchant
	import newspaper, lassie
	from newspaper import Article
	

	newspaper_data = Article(main_url)		#got data through newspaper module
	newspaper_data.download()
	newspaper_data.parse()
	newspaper_data.nlp()

	lassie_data = lassie.fetch(main_url)	#got data through lassie module

	page=urlopen(main_url)
	soup=BeautifulSoup(page,'html.parser')

	meta_properties=list()		#get a list of all meta properties available
	meta_tags=soup.find_all('meta')
	for tag in meta_tags:
		try:
			meta_properties.append(tag['property'])
		except:
			pass

	if lassie_data['title']:
		title = lassie_data['title']
	elif soup.title.text:
		title = soup.title.text
	elif soup.find("meta",{"name":"description"})['content']:
		title = soup.find("meta",{"name":"description"})['content']
	else:
		title="Unknown"

	try:
		if newspaper_data.top_image:
			icon = 	newspaper_data.top_image
		elif soup.find("meta",{"property":"og:image"})['content']:
			icon = soup.find("meta",{"property":"og:image"})['content']
	except:
		icon = "NA"

	if newspaper_data.publish_date:
		date = newspaper_data.publish_date
	elif soup.time:
		date = soup.time.text
	else:	#try to get from meta tags 	
		try:
			date_prop = ""
			date_prop_pattern = re.compile(".*date.*|.*Date.*|.*DATE.*")
			for prop in meta_properties:
				m=date_prop_pattern.search(prop)
				if m:
					date_prop=prop
					break
				else:
					pass
			date = soup.find("meta",{"property":date_prop})['content']
		except:
			date = "Unknown"

		
	a_name2 = soup.find("div", class_="author-info")		
	a_name = soup.find("div", class_="author-name")
	
	if a_name is not None:
		author_name = a_name.text
	elif a_name2 is not None:
		author_name = a_name2.text
	elif newspaper_data.authors:
		author_name = newspaper_data.authors[0]
	else:
		try:	
			author_prop = ""
			author_prop_pattern = re.compile(".*author.*|.*Author.*|.*AUTHOR.*")
			for prop in meta_properties:
				m=author_prop_pattern.search(prop)
				if m:
					author_prop=prop
					break
				else:
					pass
			author_name = soup.find("meta",{"property":author_prop})['content']
		except:
			author_name = "Unknown"
	
	lassie_desc = str(lassie_data['description'])
	newspaper_summ = str(newspaper_data.summary)
	summary = lassie_desc + newspaper_summ
	
	import category
	category = category.cate(summary)

	news_words = newspaper_data.keywords
	try:
		lassie_words = lassie_data['keywords']
		top_keywords = lassie_words
	except:
		top_keywords = list()

	for word in news_words:
		flag = 0
		for phrase in lassie_words:
			if word in phrase:
				flag = 1
				break
		if flag == 0:
			top_keywords.append(word)

	#summary,category,top_keywords=finalsumm.summ_cate(main_url,soup)
	
	"""
	keywords=list()
	title_as_list=word_tokenize(title)	
	stop_words=stopwords.words('english')
	dd = enchant.Dict("en-GB")
	for word in title_as_list:
		if (word not in stop_words and dd.check(word)):
			keywords.append(word)

	add_to_keywords=list(sorted(top_keywords, key=top_keywords.__getitem__, reverse=True))
	keywords.extend(add_to_keywords[:5])
	"""

	list_of_links = list()
	links=soup.find_all('a')
	for link in links:
		list_of_links.append(link.get('href'))

	import outlinks_classify
	same_site_links = list()
	diff_site_links = list()
	image_links = list()
	same_site_links, diff_site_links, image_links = outlinks_classify.classify(soup, main_url, list_of_links)
	return(title,date,author_name,icon,category,top_keywords,summary,same_site_links, diff_site_links, image_links)