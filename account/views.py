from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.views.decorators.http import require_POST
from .forms import LoginForm, UserRegistrationForm, UserEditForm, ProfileEditForm
from .models import Profile, Contact,LinkedinProfile,UserSubscription
from common.decorators import ajax_required
from actions.models import Action
from actions.utils import create_action
import requests
from django.utils import timezone
from django.shortcuts import render
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
import requests
import feedparser
import newspaper, lassie
from newspaper import Article
import re
from bs4 import BeautifulSoup as bs
from urllib.request import urlopen

from elasticsearch import Elasticsearch
es = Elasticsearch()

import sys
sys.path.insert(0, '/home/chivonne/Desktop/KarmaaLab/account/codes')

ENDPOINTS = {    'PEOPLE':'https://api.linkedin.com/v1/people/~',
                 'PEOPLE_SEARCH':'https://api.linkedin.com/v1/people-search',
                 'GROUPS':'https://api.linkedin.com/v1/groups',
                 'POSTS':'https://api.linkedin.com/v1/posts',
                 'COMPANIES':'https://api.linkedin.com/v1/companies',
                 'COMPANY_SEARCH':'https://api.linkedin.com/v1/company-search',
                 'JOBS':'https://api.linkedin.com/v1/jobs',
                 'JOB_SEARCH':'https://api.linkedin.com/v1/job-search'}
completeDetails = 'https://api.linkedin.com/v1/people/~:(id,first-name,email-address,last-name,headline,picture-url,industry,summary,specialties,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)),educations:(id,school-name,field-of-study,start-date,end-date,degree,activities,notes),associations,interests,num-recommenders,date-of-birth,publications:(id,title,publisher:(name),authors:(id,name),date,url,summary),patents:(id,title,summary,number,status:(id,name),office:(name),inventors:(id,name),date,url),languages:(id,language:(name),proficiency:(level,name)),skills:(id,skill:(name)),certifications:(id,name,authority:(name),number,start-date,end-date),courses:(id,name,number),recommendations-received:(id,recommendation-type,recommendation-text,recommender),honors-awards,three-current-positions,three-past-positions,volunteer)'

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Authenticated successfully')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')
    else:
        form = LoginForm()
    return render(request, 'account/login.html', {'form': form})


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)

        if user_form.is_valid():
            # Create a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # Set the chosen password
            new_user.set_password(user_form.cleaned_data['password'])
            # Save the User object
            new_user.save()
            # Create the user profile
            profile = Profile.objects.create(user=new_user)
            create_action(new_user, 'has created an account')
            return render(request,
                          'account/register_done.html',
                          {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request, 'account/register.html', {'user_form': user_form})


@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user,
                                 data=request.POST)
        profile_form = ProfileEditForm(instance=request.user.profile,
                                       data=request.POST,
                                       files=request.FILES)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Profile updated successfully')
        else:
            messages.error(request, 'Error updating your profile')
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)
    return render(request, 'account/edit.html', {'user_form': user_form,
                                                 'profile_form': profile_form})


@login_required
def dashboard(request):
    
    try:
        user = Profile.objects.get(user=request.user)
    except Profile.DoesNotExist:
        x = Profile(user=request.user)
        x.save()
        
        
    currentUser = Profile.objects.get(user=request.user)
    currentDate = timezone.now()
    expiryDateForCurrentUser = currentUser.trialEndDate
    
    subscribedModules = list()
    
    if currentDate > expiryDateForCurrentUser:
        pk = currentUser._id
        subscriptions = UserSubscription.objects.all(user=currentUser)
        if subscriptions:
            for item in subscriptions:
                subscribedModules.append(item.moduleName)
    else:
        subscribedModules = ['account/option_article.html', 'account/option_product.html', 'account/option_rss.html']
    print(subscribedModules)
    return render(request, 'account/dashboard.html', {'subscribedModules' : subscribedModules})


@login_required
def user_list(request):
    users = User.objects.filter(is_active=True)
    return render(request, 'account/user/list.html', {'section': 'people',
                                                      'users': users})


@login_required
def user_detail(request, username):
    user = get_object_or_404(User, username=username, is_active=True)
    return render(request, 'account/user/detail.html', {'section': 'people',
                                                        'user': user})

@ajax_required
@require_POST
@login_required
def user_follow(request):
    user_id = request.POST.get('id')
    action = request.POST.get('action')
    if user_id and action:
        try:
            user = User.objects.get(id=user_id)
            if action == 'follow':
                Contact.objects.get_or_create(user_from=request.user,
                                              user_to=user)
                create_action(request.user, 'is following', user)
            else:
                Contact.objects.filter(user_from=request.user,
                                       user_to=user).delete()
            return JsonResponse({'status':'ok'})
        except User.DoesNotExist:
            return JsonResponse({'status':'ko'})
    return JsonResponse({'status':'ko'})

def welcome(request):
   return render(request, 'welcome.html')

def print_summ(link):
    ns_data = Article(link)
    ns_data.download()
    ns_data.parse()
    ns_data.nlp()
    ls_data = lassie.fetch(link)
    summary = ls_data['description'] + ns_data.summary
    return summary
    #time.sleep(2)

def show_results(request):

    url_input = request.POST.get("url_input")
    url_choice = request.POST.get("url_choice")
    """
    response = requests.head(url_input)
    if response.status_code == 404:
        return HttpResponse("Sorry, this page is not found. Please try another URL.")

    elif response.status_code == 403:
        return HttpResponse("Sorry, we're not allowed access to this page. Please try another URL.")        
    """
    

    if url_choice == "Article":
        """ 
            res = es.search(index="souping", doc_type="article", body={"query": {"match": {"main_url": url_input}}})    #check if this result is already present in the database. if so, extract.
            if(res['hits']['total']!=0):
                for doc in res['hits']['hits']:
                    context = doc['_source']

            #else:    
        """
        
        import find_some_std_fields, google_custom_search
        title,date,author_name,icon,category,keywords,summary,same_site_links,diff_site_links,image_links = find_some_std_fields.mainfunc(url_input)
        search_results = google_custom_search.search_func(keywords)     #google search with the top keywords
        
        context={
            'main_url' : url_input, 
            'title' : title,        
            'date' : date,
            'author_name' : author_name,
            'icon' : icon,
            'category' : category,
            'keywords' : keywords,
            'same_site_links' : same_site_links,
            'diff_site_links' : diff_site_links,
            'image_links' : image_links,
            'summary' : summary,
            'search_results' : search_results,
        }
        
        #res = es.index(index="souping", doc_type='article', id="url_input", body=context)
        
        return render(request, 'account/show_results_article.html', context)

    elif url_choice == "Product":
        """
        res = es.search(index="souping", doc_type="product", body={"query": {"match": {"main_url": url_input}}})
        if(res['hits']['total']!=0):
           for doc in res['hits']['hits']:
                    context = doc['_source']

        else:
        """ 
        import find_some_std_fields, google_custom_search, productinfo
        title,date,author_name,icon,category,keywords,summary,same_site_links,diff_site_links,product_image_links = find_some_std_fields.mainfunc(url_input)
        price,product_id,product_description,prod_image = productinfo.mainfunc(url_input)
        search_results = google_custom_search.search_func(keywords)         


        context={
            'main_url' : url_input,
            'title' : title,
            'prod_image' : prod_image,
            #'category' : category,
            'keywords' : keywords,
            'product_id' : product_id,
            'product_image_links' : product_image_links,
            'price' : price,
            'product_description' : product_description,
            'search_results' : search_results,
        }

        #res = es.index(index="souping", doc_type='product', id="url_input", body=context)
    
        return render(request, 'account/show_results_product.html', context)

    elif url_choice == "RSS":
        list_of_titles = list()
        list_of_summaries = list()
        list_of_links = list()
        page = urlopen(url_input)
        soup = bs(page,"xml")
        items = soup.find_all("item")
        if items:
            for item in items:
                title = item.title.text
                list_of_titles.append(title)
                link = item.link.text
                list_of_links.append(link)
                summary = print_summ(link)
                list_of_summaries.append(summary)
        else:
            url = url_input + ".rss"            #Just in case "if" part doesn't work
            d = feedparser.parse(url)
            for entry in d.entries:
                title = entry['title']
                list_of_titles.append(title)
                link = entry['link']
                list_of_links.append(link)
                summary = print_summ(link)
                context[title] = summary
                list_of_summaries.append(summary)
          
        zipped_list = zip(list_of_titles, list_of_summaries, list_of_links)
        context={
                'zipped_list': zipped_list,         #*********IMPORTANT!!
            }

    
        return render(request, 'account/show_results_RSS.html', context)


#    except:
 #       return HttpResponse("Sorry. We can't find the page you're looking for. Please try again.")
