from django.conf.urls import url,include
from django.contrib.auth import views as views1
from . import views


urlpatterns = [
    # url(r'^login/$', views.user_login, name='login'),
    url(r'^$', views.dashboard, name='dashboard'),

    url(r'^register/$', views.register, name='register'),
    url(r'^edit/$', views.edit, name='edit'),

    # login / logout urls
    url(r'^login/$', views1.login ,name='login'),
    url(r'^logout/$', views1.logout, name='logout'),
    url(r'^logout-then-login/$', views1.logout_then_login, name='logout_then_login'),

    # change password urls
    url(r'^password-change/$', views1.password_change, name='password_change'),
    url(r'^password-change/done/$', views1.password_change_done, name='password_change_done'),

    # restore password urls
    url(r'^password-reset/$', views1.password_reset, name='password_reset'),
    url(r'^password-reset/done/$', views1.password_reset_done, name='password_reset_done'),
    url(r'^password-reset/confirm/(?P<uidb64>[-\w]+)/(?P<token>[-\w]+)/$', views1.password_reset_confirm, name='password_reset_confirm'),
    url(r'^password-reset/complete/$', views1.password_reset_complete, name='password_reset_complete'),

    # user profiles
    url(r'^users/$', views.user_list, name='user_list'),
    url(r'^users/follow/$', views.user_follow, name='user_follow'),
    url(r'^users/(?P<username>[-\w]+)/$', views.user_detail, name='user_detail'),

]
