from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.utils import timezone
import datetime


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    date_of_birth = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to='users/%Y/%m/%d', blank=True)
    joinedDate = models.DateTimeField(default=timezone.now())
    trialEndDate = models.DateTimeField(default=(timezone.now()+datetime.timedelta(days=7)))

    def __str__(self):
        return 'Profile for user {}'.format(self.user.username)
    
class UserSubscription(models.Model):
    moduleName = models.CharField(max_length=200)
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    beginDate = models.DateField(default=timezone.now())
    endDate  = models.DateTimeField(default=timezone.now())


class Contact(models.Model):
    user_from = models.ForeignKey(User,related_name='rel_from_set')
    user_to = models.ForeignKey(User, related_name='rel_to_set')
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return '{} follows {}'.format(self.user_from, self.user_to)
    
class LinkedinProfile(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField()
    headline = models.CharField(max_length=200)
    industry = models.CharField(max_length=300)
    pictureUrl = models.URLField()


# Add following field to User dynamically
User.add_to_class('following',
                  models.ManyToManyField('self',
                                         through=Contact,
                                         related_name='followers',
                                         symmetrical=False))
